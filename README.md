# Scrabble Solver

This is a simple but effective SPA for finding the best scrabble move given a board pattern, using haskell + \_hyperscript.

Currently in progress with an extremely rudimentary UI, but basic functionality is already there.
Essentially my approach is as follows:

1. Generate a tree which indexes sets of scrabble words according to the frequency of each letter they contain.
2. Once generated, start a server which serves a basic UI and waits for requests for words according to a set of characters.
3. Find that set, and filter it by the given board pattern (e.g. \_\_\_H_L\_ could fit 'HILL' or 'ITCH').
4. Display the results to the user.

## How to Run

Not ready/optimised for production use yet, but a development server can be started by executing the `runDev` script. Only tested on Linux.

Requirements:

- `stack` and `browser-sync` installations.
- Run `stack build` and then `stack install yesod-bin` within directory.
- Run the script `runDev` in the top-level directory. After compilation, a locally hosted server should be available at https://localhost:4000 and it should re-compile and reload after every source change.
