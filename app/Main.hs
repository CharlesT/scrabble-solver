module Main where

import Protolude
import Server.Server (startServer)
import qualified Web.Scotty as S
import WordTree.FreqTree (mkFTree)
import WordTree.ReadWords (readWordFile)

main :: IO ()
main = do
  allWords <- readWordFile
  let eTree = mkFTree allWords
  S.scotty 3000 $ startServer eTree
