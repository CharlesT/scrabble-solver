-- |
module WordTreeSpec
  ( wordTest,
    searchTest,
    leewaySearchTest,
  )
where

import qualified Data.Set as S
import Protolude
import Test.Hspec
import WordTree.FreqTree (leewayLookup, lookup, mkFTree)
import WordTree.ReadWords (readWordFile)
import WordTree.Words
  ( alphaNum,
    mkCharCounts,
    mkScrabbleWord,
  )

-- | Character counting
wordTest :: IO ()
wordTest = hspec $ do
  describe "alphaNum" $ do
    it "a -> 0" $ do
      alphaNum 'a' `shouldBe` 0
    it "b -> 1" $ do
      alphaNum 'b' `shouldBe` 1
    it "z -> 25" $ do
      alphaNum 'z' `shouldBe` 25

  describe "mkCharCounts" $ do
    it "Characters are counted correctly" $ do
      mkCharCounts "aabccdxyzz"
        `shouldBe` zip
          ['a' .. 'z']
          ( [2, 1, 2, 1]
              ++ replicate 19 0
              ++ [1, 1, 2]
          )

-- | Utility function for converting a type into a Text.
tshow :: (Show a) => a -> Text
tshow x = toS (show x :: [Char])

-- | Strict lookups for exact character matches.
searchTest :: IO ()
searchTest = do
  allWords <- readWordFile
  let eTree = first tshow $ mkFTree allWords
  hspec $ do
    describe "Basic searching" $ do
      let charSets =
            [ ("bad", ["bad", "dab"]),
              ("bad", ["bad", "dab"]),
              ("ant", ["ant", "tan", "nat"]),
              ("nope", ["nope", "peon", "open", "pone"]),
              ("gzt", [])
            ]
      forM_ charSets $ \(chars, results) -> do
        it ("Letters: " ++ chars) $ do
          let result = do
                tree <- eTree
                first tshow $ lookup tree chars
          result `shouldBe` Right (S.fromList (mkScrabbleWord <$> results))

-- | More lenient lookups for character matches with 1 or 2 degrees of leeway.
leewaySearchTest :: IO ()
leewaySearchTest = do
  allWords <- readWordFile
  let eTree = first tshow $ mkFTree allWords
  hspec $ do
    describe "Leeway = 1 searching" $ do
      let charSets =
            [ ("mall", ["mall", "all"]),
              ("feign", ["feign", "fine"])
            ]
      forM_ charSets $ \(chars, results) -> do
        it ("Letters: " ++ chars) $ do
          let result = do
                tree <- eTree
                first tshow $ leewayLookup 1 tree chars
          result
            `shouldSatisfy` either
              (const False)
              (S.isSubsetOf $ S.fromList (mkScrabbleWord <$> results))

    describe "Leeway = 2 searching" $ do
      let charSets =
            [ ("mall", ["mall", "all", "am"]),
              ("feign", ["feign", "fine", "gin", "fin", "neg", "gif", "gen"])
            ]
      forM_ charSets $ \(chars, results) -> do
        it ("Letters: " ++ chars) $ do
          let result = do
                tree <- eTree
                first tshow $ leewayLookup 2 tree chars
          result
            `shouldSatisfy` either
              (const False)
              (S.isSubsetOf $ S.fromList (mkScrabbleWord <$> results))
