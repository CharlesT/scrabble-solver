-- |
module Spec where

import Protolude
import WordTreeSpec

main :: IO ()
main = do
  wordTest
  searchTest
  leewaySearchTest
