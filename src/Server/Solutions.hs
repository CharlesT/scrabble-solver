--

module Server.Solutions (results) where

import Protolude
import Text.Blaze.Html ((!))
import qualified Text.Blaze.Html5 as H
import Text.Blaze.Html5.Attributes
import WordTree.Words (ScrabbleWord (..))

results :: [(ScrabbleWord, Text)] -> H.Html
results scrabbleWords = H.table ! class_ "table border-collapse table-auto w-4/6" $ do
  H.thead ! class_ "table-header-group" $
    H.tr ! class_ "table-row" $ do
      for_ ["Word", "Score", "Definition"] $ \header ->
        H.th ! class_ "table-cell border-b p-2" $ H.toHtml @Text header
  H.tbody ! class_ "table-row-group" $
    for_ scrabbleWords $ \(word, definition) ->
      H.tr ! class_ "table-row" $ do
        for_
          [ H.toHtml (getWord word),
            H.toHtml (getScore word),
            H.toHtml definition
          ]
          $ \cellContent ->
            H.td ! class_ "table-cell border-b p-2" $ cellContent
