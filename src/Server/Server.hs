{-# LANGUAGE OverloadedStrings #-}

-- | Provide an entry point into the scotty server
module Server.Server (startServer, develStartServer) where

import qualified Data.Set as Set
import qualified Data.Text.Lazy as TIO
import Network.Wai.Middleware.Static
import Protolude
import Server.Solutions (results)
import Server.Tile (boardSlice, hand)
import System.Directory (doesFileExist)
import System.Environment
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Blaze.Html5 as H
import Text.Blaze.Html5.Attributes
import Text.Blaze.Htmx (hxPost, hxSwap, hxTarget)
import qualified Web.Scotty as S
import WordTree.BoardMatch (fits, mkPattern, noFitPattern)
import WordTree.FreqTree (ScrabbleTree, leewayLookup, mkFTree)
import WordTree.ReadWords (readWordFile)
import WordTree.Words (ScrabbleWord)
import Prelude (read)

develStartServer :: IO ()
develStartServer = do
  race_ watchTermFile $ do
    port <- read @Int <$> getEnv "PORT"
    displayPort <- getEnv "DISPLAY_PORT"
    putStrLn $ "Running in development mode on port " ++ show port
    putStrLn $ "But you should connect to port " ++ displayPort
    -- allWords <- readWordFile "res/scrabble_words.txt"
    allWords <- readWordFile "res/simple_words.txt"
    let eTree = mkFTree allWords
    S.scotty port $ startServer eTree

startServer :: Either a ScrabbleTree -> S.ScottyM ()
startServer eTree = do
  S.middleware $ staticPolicy $ noDots >-> addBase "static"
  case eTree of
    Left _ -> S.get "/" $ do
      S.html "FAILED TO TREE"
    Right tree -> do
      S.get "/" $
        S.html $
          renderHtml page
      -- H.input ! type_ "text" ! name "chars"
      -- H.input ! type_ "number" ! name "leeway"
      -- H.input ! type_ "submit"
      S.post "/search" $ do
        -- S.liftAndCatchIO $ threadDelay 2000000
        allInputs <- S.params
        let chars = fmap snd $ filter ((== "chars") . fst) allInputs
            boardSpots =
              TIO.pack $
                fromMaybe '_' . fmap fst . TIO.uncons
                  <$> snd
                  <$> filter ((== "board") . fst) allInputs

        case leewayLookup (length chars) tree (TIO.unpack $ mconcat chars) of
          Left _ -> S.html "Lookup failed!"
          Right wordSet -> S.html . renderHtml $ do
            H.script "" ! src "https://cdn.tailwindcss.com"
            if Set.null wordSet
              then H.span "Nothing there..."
              else
                results $
                  let words = Set.toDescList wordSet
                      boardPattern = fromRight noFitPattern $ mkPattern $ TIO.toStrict boardSpots
                      filteredWords = filter (`fits` boardPattern) words
                   in (,"This will be the definition, once I figure out how to get it.")
                        <$> filteredWords

page :: H.Html
page = H.html $ do
  -- H.link ! rel "stylesheet" ! href "/styles.css"
  H.head $ do
    H.script "" ! src "https://cdn.tailwindcss.com"
    H.script "" ! src "https://unpkg.com/hyperscript.org@0.9.5"
    H.script "" ! src "https://unpkg.com/htmx.org@1.7.0"
  H.body ! class_ "w-full flex justify-evenly items-center flex-col" $ do
    H.form
      ! class_ "flex flex-col min-h-[40vh] max-h-full justify-evenly items-center"
      ! hxPost "/search"
      ! hxTarget "#search-results"
      -- ! dataAttribute "hx-indicator" "#search-results"
      ! hxSwap "innerHTML"
      $ do
        hand ! class_ "flex-1"
        boardSlice ! class_ "flex-1"
        H.button "Search away!" ! type_ "submit"
          ! class_ "rounded-full bg-teal-300 p-3 shadow-md"
    H.img ! src "/rings.svg" ! class_ "hidden w-1/6"
      ! customAttribute "_" "on htmx:beforeRequest add .inline on htmx:afterRequest add .hidden"
    H.div ""
      ! id "search-results"
      ! class_ "flex justify-evenly items-center flex-col"

-- | Would certainly be more efficient to use fsnotify, but this is
-- simpler.
watchTermFile :: IO ()
watchTermFile = do
  exists <- doesFileExist "yesod-devel/devel-terminate"
  if exists
    then return ()
    else do
      threadDelay 100000
      watchTermFile
