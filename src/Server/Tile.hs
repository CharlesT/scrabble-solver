-- | Single character tile
module Server.Tile where

import Protolude
import Text.Blaze.Html5 as H
import Text.Blaze.Html5.Attributes

data TyleStyle = Sunken | Floating
  deriving (Eq)

tile :: TyleStyle -> AttributeValue -> H.Html
tile tileStyle formName =
  H.input ! type_ "text" ! maxlength "1" ! name formName
    ! customAttribute
      "_"
      "on keyup call document.activeElement.nextElementSibling.focus()\
      \ on focus call document.activeElement.select()"
    ! class_
      ( "mx-auto border-x-teal border-1 text-center text-xl shadow-slate-500 "
          <> case tileStyle of
            Sunken -> "shadow-inner w-20 h-20"
            Floating -> "shadow w-12 h-12 bg-amber-200 rounded"
      )

btnAddTile :: TyleStyle -> Text -> H.Html
btnAddTile tileStyle char =
  H.button (H.toHtml char)

hand :: H.Html
hand = H.div ! class_ "space-x-4" $ do
  replicateM_ 7 $ tile Floating "chars"
  btnAddTile Floating "+"

boardSlice :: H.Html
boardSlice = H.div ! class_ "space-x-4" $ do
  btnAddTile Sunken "+"
  replicateM_ 8 $ tile Sunken "board"
  btnAddTile Sunken "+"
