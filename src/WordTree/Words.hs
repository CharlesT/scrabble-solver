{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}

-- |
-- Module      : Utils.Vector
-- Description : Vector types customised for global usage in this project.
-- Copyright   : (c) Charles Taylor, 2022
-- License     : GPL-3
-- Maintainer  : taylorc822@gmail.com
-- Stability   : experimental
-- Portability : POSIX
--
-- TODO Here is a longer description of this module, containing some
-- commentary with @some markup@.
module WordTree.Words
  ( ScrabbleWord,
    mkScrabbleWord,
    getWord,
    getCharPositions,
    getScore,
    getLength,
    mkCharPositionMap,
  )
where

import Data.Bitraversable (Bitraversable (bitraverse))
import qualified Data.Set as S
import qualified Data.Text as T
import GHC.Show (Show (..))
import Protolude
import qualified WordTree.AlphaMap as A

data ScrabbleWord = UnsafeMkScrWord
  { getWord :: Text,
    getCharPositions :: A.AlphaMap (S.Set Int),
    getScore :: Int,
    getLength :: Int
  }
  deriving (Generic)

-- | Custom instance for efficiency
instance Eq ScrabbleWord where
  (==) = on (==) getWord

-- | Custom instance to allow primarily sorting by score.
instance Ord ScrabbleWord where
  compare
    (UnsafeMkScrWord word1 _ score1 _)
    (UnsafeMkScrWord word2 _ score2 _) =
      if score1 == score2
        then compare word1 word2
        else compare score1 score2

instance Show ScrabbleWord where
  show = toS . getWord

-- -- | Note that this means words cannot have more than 10 of a letter,
-- -- and a list of characters cannot have more than 10 characters.
-- type TileCount = Finite 10

mkScrabbleWord :: Text -> ScrabbleWord
mkScrabbleWord txt =
  UnsafeMkScrWord
    (T.toUpper txt)
    (mkCharPositionMap $ T.unpack txt)
    (wScore $ T.unpack txt)
    (T.length txt)

-- | Group the positions of each character together according
-- to the character, in an AlphaMap.
mkCharPositionMap :: [Char] -> A.AlphaMap (S.Set Int)
mkCharPositionMap =
  flip (A.accum S.union) A.empty
    . mapMaybe (bitraverse A.mkAlpha Just)
    . flip zip (S.singleton <$> [0 ..])

wScore :: [Char] -> Int
wScore chs =
  mapMaybe A.mkAlpha chs
    & fmap (A.lookup scores)
    & sum

filterWordPattern :: MonadPlus m => [(Int, Char)] -> m ScrabbleWord -> m ScrabbleWord
filterWordPattern = mfilter . fitsAroundExistingTiles

fitsAroundExistingTiles :: [(Int, Char)] -> ScrabbleWord -> Bool
fitsAroundExistingTiles chars word = True

scores :: A.AlphaMap Int
scores =
  A.fromListWithDefault 0 $
    zip
      A.alphaList
      [ 1,
        3,
        3,
        2,
        1,
        4,
        2,
        4,
        1,
        8,
        5,
        1,
        3,
        1,
        1,
        3,
        10,
        1,
        1,
        1,
        1,
        4,
        4,
        8,
        4,
        10
      ]
