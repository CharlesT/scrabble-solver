{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}

-- |
module WordTree.FreqTree
  ( TreeBuildError (..),
    LookupError (..),
    ScrabbleTree,
    FreqTree,
    mkFTree,
    lookup,
    leewayLookup,
    emptyTree,
  )
where

import qualified Data.Map as M
import qualified Data.Set as S
import Protolude
import qualified WordTree.AlphaMap as A
import WordTree.Words (ScrabbleWord, getCharPositions, mkCharPositionMap)

type CharCount = (A.AlphaChar, Int)

data FreqTree a = FTree (M.Map CharCount (FreqTree a)) | FLeaf (S.Set a)
  deriving (Show, Eq)

type ScrabbleTree = FreqTree ScrabbleWord

data TreeBuildError a
  = BranchTooLong a
  | BranchWasBuiltTooShort a
  deriving (Eq, Show)

-- | Tree of Scrabble Words which may have failed while building.
type TreeBuild = Either (TreeBuildError [Char]) ScrabbleTree

-- | Non-leaf variant of an empty tree (contains an empty Map)
emptyTree :: FreqTree a
emptyTree = FTree mempty

-- | Build a Tree of ScrabbleWords from a list of pre-parsed words.
mkFTree :: [ScrabbleWord] -> TreeBuild
mkFTree = foldl' (flip insertWord) (Right $ FTree mempty)

-- | Insert a single ScrabbleWord into a ScrabbleTree, indexed according to the
--  frequency of each alphabetical character found in the given word.
insertWord :: ScrabbleWord -> TreeBuild -> TreeBuild
insertWord = liftA2 insert' (A.toList . fmap S.size . getCharPositions) identity
  where
    insert' :: [(A.AlphaChar, Int)] -> ScrabbleWord -> TreeBuild -> TreeBuild
    insert' [] word = (=<<) $ \case
      FLeaf vals -> Right $ FLeaf $ S.insert word vals
      FTree treeMap ->
        if M.null treeMap
          then Right $ FLeaf $ S.singleton word
          else Left $ BranchTooLong $ show word ++ ",  Map: " ++ show treeMap
    insert' (x : xs) word = (=<<) $ \case
      FLeaf v -> Left $ BranchWasBuiltTooShort $ show word ++ ",  Iterations so far: " ++ show (26 - length xs) ++ ",  vals: " ++ show v
      FTree treeMap -> do
        innerTree <- insert' xs word $ Right $ fromMaybe emptyTree $ M.lookup x treeMap
        Right . FTree $ M.insert x innerTree treeMap

data LookupError a
  = CharCountTooLong a
  | CharCountTooShort a
  deriving (Eq, Show)

-- | Set of ScrabbleWords matching a lookup, with the possibility of error if the
--  given CharCount is the wrong length.
type LookupResult = Either (LookupError [Char]) (S.Set ScrabbleWord)

-- | A lookup with no leeway: resultant words must use exactly all of the
-- given characters.
lookup :: ScrabbleTree -> [Char] -> LookupResult
lookup = leewayLookup 0

-- | Find all ScrabbleWords which contain the given list of characters, with the
-- caveat that `leeway` number of characters can be ignored.
leewayLookup :: Int -> ScrabbleTree -> [Char] -> LookupResult
leewayLookup leeway tree chars = pLookup leeway (A.toList $ S.size <$> mkCharPositionMap chars) tree
  where
    pLookup :: Int -> [(A.AlphaChar, Int)] -> ScrabbleTree -> LookupResult
    pLookup _ [] = \case
      FLeaf results -> Right results
      FTree map -> Left $ CharCountTooShort $ show map
    pLookup leeway ((ch, count) : indices) = \case
      FLeaf set -> Left $ CharCountTooLong $ show set
      FTree map ->
        let emptyResult = Right S.empty
            noLeewayResult =
              maybe
                emptyResult
                (pLookup leeway indices)
                (M.lookup (ch, count) map)
            leewayResults =
              maybe
                emptyResult
                (pLookup (leeway -1) indices)
                <$> (flip M.lookup map . (ch,) <$> leeway `under` count)
         in if count <= 0
              then noLeewayResult
              else fmap fold $ sequenceA $ noLeewayResult : leewayResults

-- | Get a list of the positive numbers which are `x` below `y`.
under :: Int -> Int -> [Int]
under x y = filter (>= 0) [y - x .. y -1]
