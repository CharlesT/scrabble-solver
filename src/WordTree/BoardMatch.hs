{-# LANGUAGE FlexibleContexts #-}

-- | Module grouping parser combinators intended to check if a scrabble
-- word matches a pattern on a board.
-- E.g. both 'chain' and 'she' would match _H__N_
module WordTree.BoardMatch (mkPattern, fits, type Pattern, noFitPattern) where

import Control.Monad.Except
import Control.Monad.State
import Control.Monad.Tardis (MonadTardis (getFuture, getPast, sendFuture, sendPast), evalTardisT, modifyBackwards, modifyForwards)
import qualified Data.Map as M
import qualified Data.Sequence as S
import qualified Data.Sequence as Seq
import qualified Data.Set as Set
import qualified Data.Text as T
import Protolude
import qualified WordTree.AlphaMap as A
import WordTree.Words (ScrabbleWord (getCharPositions, getLength))

type Position = Int

type Length = Int

newtype Pattern = Pat
  { pat :: M.Map Length [Match]
  }
  deriving (Eq, Show)

patternsForLen :: Length -> Pattern -> Either Text [Match]
patternsForLen len (Pat pMap) = maybeToEither "This length is invalid" $ pMap M.!? len

-- | Take a Text composed of alphabetic characters and '_' characters,
-- and return all possible places a word would go.
--
-- Examples:
--
-- >>> mkPattern "__H____J_" >>= patternsForLen 2
-- Right [[(AlphaChar 'H',1)],[(AlphaChar 'H',0)],[(AlphaChar 'J',1)],[(AlphaChar 'J',0)]]
--
-- >>> mkPattern "_IH_" >>= patternsForLen 2
-- Right []
--
-- >>> mkPattern "__I_H__" >>= patternsForLen 3
-- Right [[(AlphaChar 'I',2)],[(AlphaChar 'H',0)]]
--
-- >>> mkPattern "H" >>= patternsForLen 2
-- Left "This length is invalid"
mkPattern :: Text -> Either Text Pattern
mkPattern txt = do
  contextList <- buildContextList txt
  return . Pat . M.fromList $
    (,) <$> identity <*> patternForLen contextList <$> [2 .. T.length txt]

noFitPattern :: Pattern
noFitPattern = Pat M.empty

type Match = [(A.AlphaChar, Position)]

-- | Find all relevant patterns for a given length
--
-- Examples:
--
-- >>> buildContextList "_H_" >>= (\list -> Right $ patternForLen list 2)
-- Right [[(AlphaChar 'H',1)],[(AlphaChar 'H',0)]]
--
-- >>> buildContextList "__H_" >>= (\list -> Right $ patternForLen list 2)
-- Right [[(AlphaChar 'H',1)],[(AlphaChar 'H',0)]]
--
-- >>> buildContextList "____" >>= (\list -> Right $ patternForLen list 2)
-- Right []
--
-- >>> buildContextList "__H____J_" >>= (\list -> Right $ patternForLen list 5)
-- Right [[(AlphaChar 'H',2)],[(AlphaChar 'H',1)],[(AlphaChar 'J',3)]]
patternForLen :: [CharContext] -> Length -> [Match]
patternForLen contextList len = do
  ch <- contextList
  pos <- possiblePositions len ch
  return $ second (const pos) <$> match ch

possiblePositions :: Length -> CharContext -> [Position]
possiblePositions len (CharContext l _ r) =
  let availableLeft = min l (len - 1)
      availableRight = min r (len - 1)
      -- Technically this is the number of possible iterations less 1,
      -- but it's convenient to calculate this way.
      iterations = (availableLeft + availableRight + 1) - len
   in (availableLeft -) <$> [0 .. iterations]

data CharContext = CharContext
  { left :: Length,
    match :: Match,
    right :: Length
  }
  deriving (Eq, Show)

-- | I get to use the Tardis monad!!! I probably could have done without,
-- but I feel so cool and trendy!! It gives me access to the number of '_'
-- characters on either side of the current character without pre-processing.
--
-- Examples:
--
-- >>> buildContextList "_H___H"
-- Right [AroundChar {left = 1, char = AlphaChar 'H', position = 2, right = 2},AroundChar {left = 2, char = AlphaChar 'H', position = 6, right = 0}]
--
-- >>> buildContextList "_JK_"
-- Right [AroundChar {left = 1, char = AlphaChar 'J', position = 2, right = -1},AroundChar {left = -1, char = AlphaChar 'K', position = 3, right = 1}]
--
-- >>> buildContextList "_*"
-- Left "Unexpected character in pattern"
--
-- >>> buildContextList "___"
-- Right []
buildContextList :: Text -> Either Text [CharContext]
buildContextList txt = flip evalTardisT (0, 0) $
  fmap catMaybes $ -- Removes the '_' characters
    for (zip [1 ..] $ T.unpack txt) $
      \(pos, ch) -> case A.mkAlpha ch of
        Nothing ->
          if ch /= '_'
            then lift (Left "Unexpected character in pattern")
            else do
              modifyBackwards (+ 1)
              modifyForwards (+ 1)
              return Nothing
        Just alphCh -> do
          -- '-1' is used here to indicate invalid space,
          -- as there is a character immediately adjacent.
          sendPast (-1)
          next <- getFuture
          prev <- getPast
          sendFuture (-1)

          return . Just $ CharContext prev [(alphCh, pos)] next

-- matchesPattern :: [Pattern] -> Text -> Bool
-- matchesPattern pat txt = case parse parser txt of
--   Done _ result -> result `fits` expandedPattern
--   _ -> False

fits :: ScrabbleWord -> Pattern -> Bool
fits word (Pat fullPat) =
  let alphaMap = getCharPositions word
      relevantPat = fromMaybe [] $ M.lookup (getLength word) fullPat
   in (any . any)
        ( \(ch, i) ->
            let charPositionSet = A.lookup alphaMap ch
             in Set.member i charPositionSet
        )
        relevantPat

-- parsePattern :: VU.Vector (Maybe Char) -> Vector [[(Char, Int)]]
-- parsePattern inputPattern = undefined

-- findProximities :: VU.Vector (Maybe Char) ->
