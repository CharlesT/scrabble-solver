--
module WordTree.ReadWords (readWordFile) where

import Control.Monad.Except
import Protolude
import WordTree.Words (ScrabbleWord, mkScrabbleWord)

-- | Read in a list of scrabble words, expected to be located
-- at res/scrabble_words.txt.
readWordFile :: FilePath -> IO [ScrabbleWord]
readWordFile filename = do
  fmap mkScrabbleWord . words <$> readFile filename
