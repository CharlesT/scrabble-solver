{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- | Data structure for O(1) access to values indexed by letters
-- of the english alphabet.
module WordTree.AlphaMap
  ( type AlphaChar,
    type AlphaMap,
    mkAlpha,
    alphaList,
    fromList,
    fromListWithDefault,
    toList,
    accum,
    update,
    empty,
    repeated,
    lookup,
  )
where

import Data.Char
import Data.Ix
import qualified Data.Vector as V
import Protolude hiding (empty, toList)

-- | Type to numerically represent the upper-case letters of the alphabet.
newtype AlphaChar = AlphaChar Char
  deriving
    ( Eq,
      Ord,
      Show,
      Read,
      Bounded,
      Enum,
      Ix
    )

newtype AlphaMap a = AlphaMap (V.Vector a)
  deriving
    ( Eq,
      Show,
      Functor,
      Foldable,
      Traversable
    )

mkAlpha :: Char -> Maybe AlphaChar
mkAlpha ch =
  if inRange ('A', 'Z') ch
    then Just (AlphaChar ch)
    else Nothing

alphaList :: [AlphaChar]
alphaList = AlphaChar <$> ['A' .. 'Z']

alphaNum :: AlphaChar -> Int
alphaNum (AlphaChar ch) = ord ch - ord 'A'

empty :: Monoid a => AlphaMap a
empty = AlphaMap $ V.replicate 26 mempty

repeated :: a -> AlphaMap a
repeated = AlphaMap . V.replicate 26

accum :: (a -> b -> a) -> [(AlphaChar, b)] -> AlphaMap a -> AlphaMap a
accum f chs (AlphaMap vec) =
  let updates = first alphaNum <$> chs
   in AlphaMap $ V.accum f vec updates

update :: [(AlphaChar, a)] -> AlphaMap a -> AlphaMap a
update chs (AlphaMap vec) =
  let updates = first alphaNum <$> chs
   in AlphaMap $ V.accum (\_ a -> a) vec updates

fromList :: (Monoid a) => [(AlphaChar, a)] -> AlphaMap a
fromList xs = xs `update` empty

fromListWithDefault :: a -> [(AlphaChar, a)] -> AlphaMap a
fromListWithDefault defaultVal xs = xs `update` repeated defaultVal

toList :: AlphaMap a -> [(AlphaChar, a)]
toList (AlphaMap vec) =
  V.toList vec
    & zip alphaList

-- | Return the values associated with the given character, assuming
-- it exists. Use of partial function (!) is permisable as the limited
-- scope of AlphaChar guarantees a match.
--
-- Examples:
--
-- >>> mkAlpha 'B' >>= lookup (fromListWithDefault Nothing [(AlphaChar 'B', Just (7 :: Int))])
-- Just 7
--
-- >>> mkAlpha 'B' >>= lookup (fromListWithDefault Nothing [(AlphaChar 'C', Just (7 :: Int))])
-- Nothing
lookup :: AlphaMap a -> AlphaChar -> a
lookup (AlphaMap vec) = (vec V.!) . alphaNum
