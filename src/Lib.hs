module Lib
  ( someFunc,
  )
where

import Protolude

someFunc :: IO ()
someFunc = putStrLn @Text "Not defined yet"
